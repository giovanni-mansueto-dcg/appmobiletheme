{% assign ngBaseModel = "profileRegister." %}
{% assign CustomerFieldsNotDisplayed = "Name,Surname,Cpf,Cnpj,Email,BirthDate,Gender,Password,TradingName,AddOrSetCustomer.Gender,gender" %}

<div class="alert alert-danger" role="alert" data-ng-show="!documentIsValid">
	O documento informado não é valido
</div>

{% assign submitMethod = 'signUp' %}

{% if Widget.IsEditMode %}
	{% assign submitMethod = 'edit' %}
{% endif %}

<form name="form" data-ng-submit="{{ submitMethod }}(form, $event)" class="feedback-icons" data-ng-model-options="{ updateOn: 'blur' }" novalidate>

	<div data-ng-show="requesting" class="card-validation col-md-12" aria-hidden="true">

		<div class="bg-check-card"></div>
		<div data-ng-show="!requestingDone" class="progress-circle-indeterminate progress-circle-success check-card"></div>
		<i data-ng-show="requestingDone" class="fa fa-check-circle check-complete"></i>

	</div>

	<input type="hidden" name="AddOrSetCustomer.CustomerType" value="" data-ng-value="customerType">

	<div class="row cpf email">

		<div class="form-group col-md-6 col-sm-12">
			<label for="{% raw %}{{ documentFieldName }}{% endraw %}">CPF/CNPJ</label>
			<input type="text" id="{% raw %}{{ documentFieldName }}{% endraw %}" name="{% raw %}{{ documentFieldName }}{% endraw %}" class="form-control" maxlength="18" data-ng-model-options="{ updateOn: 'blur', allowInvalid: true }" data-ng-model="{{ ngBaseModel }}document" data-validate-document data-mask-document required>
		</div>

		{% metadata_for(Entity="Person", Only="Email", DisplayName="E-mail", InputPrefix="AddOrSetCustomer", InputSize=40, WrapperClass="form-group col-md-6 col-sm-12", NgModel: ngBaseModel ) %}
		{% metadata_for(Entity="Company", Only="Email", DisplayName="E-mail", InputPrefix="AddOrSetCustomer", InputSize=40, WrapperClass="form-group col-md-6 col-sm-12", NgModel: ngBaseModel ) %}

	</div>

	<div class="row name surname">

		{% metadata_for(Entity="Person", Only="Name", DisplayName="Nome", InputPrefix="AddOrSetCustomer", InputSize=40, WrapperClass="form-group col-md-6 col-sm-12", NgModel: ngBaseModel ) %}
		{% metadata_for(Entity="Company", Only="Name", DisplayName="Nome", InputPrefix="AddOrSetCustomer", InputSize=40, WrapperClass="form-group col-md-6 col-sm-12", NgModel: ngBaseModel ) %}
		{% metadata_for(Entity="Person", Only="Surname", DisplayName="Sobrenome", InputPrefix="AddOrSetCustomer", InputSize=40, WrapperClass="form-group col-md-6 col-sm-12", NgModel: ngBaseModel ) %}
		{% metadata_for(Entity="Company", Only="TradingName", InputPrefix="AddOrSetCustomer", InputSize=40, WrapperClass="form-group col-md-6 col-sm-12", NgModel: ngBaseModel ) %}

	</div>

	<div class="person-extended" data-ng-if="customerType === 'Person'">

		{% metadata_component(Template="wd.metadata.component.for.template", Entity= "Person", RequiredOnly=true, Except:CustomerFieldsNotDisplayed, NgModel: ngBaseModel, NotRenderHtml=true) %}

	</div>

	<div class="company-extended" data-ng-if="customerType === 'Company'">

		{% metadata_component(Template="wd.metadata.component.for.template", Entity= "Company", RequiredOnly=false, Except:CustomerFieldsNotDisplayed, NgModel: ngBaseModel, NotRenderHtml=true) %}

	</div>

	{% assign BirthDateCustomProp= 'data-is-valid-date data-ng-model-options="{ allowInvalid: true }"' %}

	<div class="row birthdate gender">

		{% metadata_for(Entity="Person", Only="BirthDate", DisplayName="Data de nascimento", InputPrefix="AddOrSetCustomer", InputSize=10, WrapperClass="form-group col-md-6 col-sm-12", NgModel: ngBaseModel, CustomProp: BirthDateCustomProp ) %}
		{% metadata_for(Entity="Company", Only="BirthDate", DisplayName="Data de nascimento", InputPrefix="AddOrSetCustomer", InputSize=10, WrapperClass="form-group col-md-6 col-sm-12", NgModel: ngBaseModel, CustomProp: BirthDateCustomProp ) %}
		{% metadata_for(Entity="Person", Only="Gender", DisplayName="Sexo", InputPrefix="AddOrSetCustomer", InputSize=40, WrapperClass="form-group col-md-6 col-sm-12", NgModel: ngBaseModel ) %}
		{% metadata_for(Entity="Company", Only="Gender", DisplayName="Sexo", InputPrefix="AddOrSetCustomer", InputSize=40, WrapperClass="form-group col-md-6 col-sm-12", NgModel: ngBaseModel ) %}

	</div>

	<input type="hidden" name="AddOrSetCustomer.GenerateAndDefinePassword" value="True">

	{% comment %}

		#CORE-13996

		{% unless Widget.IsEditMode %}

			<div class="row">

				<div data-ng-click="showExtendedFields = !showExtendedFields" class="col-md-12 text-center open-complementary-info">

					<i data-ng-show="!showExtendedFields" class="fa fa-chevron-down"></i>
					<i data-ng-show="showExtendedFields" class="fa fa-chevron-up"></i>

					{% raw %}
						<span data-ng-if="!showExtendedFields">Inserir informações complementares</span>
						<span data-ng-if="showExtendedFields">Ocultar informações complementares</span>
					{% endraw %}

				</div>

			</div>

		{% endunless %}

	{% endcomment %}

	{% unless Widget.IsEditMode %}
		<div data-ng-if="showExtendedFields" class="complementary-info">
	{% endunless %}

	{% assign AddressFieldsNotDisplayed = "AddressLine,Number,AddressNotes,Neighbourhood,PostalCode,City,State" %}
	{% assign AddressCustomProp = 'data-ng-if="showExtendedFields"' %}

	{% metadata_component(Template="wd.metadata.component.for.template" Entity= "Person", OptionalOnly=true, Except:CustomerFieldsNotDisplayed, Order:Widget.FieldsOrderDisplayedToPerson, NgModel: ngBaseModel, CustomProp: AddressCustomProp, NotRenderHtml=true) %}

	{% unless Widget.SkipAddress %}

		<h5 class="semi-bold">Endereço</h5>
		{% profile_addressform(Template="wd.profile.addressform.fields.template") %}

	{% endunless %}

	{% unless Widget.IsEditMode %}
		</div>
	{% endunless %}

	<hr>

	{% if Widget.IsEditMode %}

		<div class="row edit-button-wrapper">

			<div class="form-group clearfix col-md-6">
				<button data-ng-click="cancel($event);" class="btn btn-default btn-block close-edit">Cancelar</button>
			</div>

			<div class="form-group clearfix col-md-6">
				<button data-pos-loader="requesting" type="submit" class="btn btn-success add-client btn-block">Salvar informações</button>
			</div>

		</div>

	{% else %}

		<div class="form-group clearfix text-center">
			<button data-ng-click="trySubmit()" type="submit" class="btn btn-success add-client" data-ng-disabled="requesting">Cadastrar novo cliente</button>
		</div>

	{% endif %}

</form>

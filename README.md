# Theme: PoS (Point of Sale)

## UI Framework

### Pages

- [Live preview](http://pages.revox.io/dashboard/latest/html/)
- [Documentation](http://pages.revox.io/dashboard/latest/doc/)
- [Browser Support](http://pages.revox.io/dashboard/latest/doc/partials/browser_support.html)

## Development

### TL;DR

```powershell
npm install -g plato karma-cli phantomjs-prebuilt

cd C:\Projects\Core.Dev\code\EZ.Store.Custom\Content\Themes\POS
npm install
npm run tdd
gulp

# Tunnel at xip.io and localtunnel.me
gulp --tunnel

# Without JSEL logs
gulp --without-logs
```

#### Tests

```powershell
npm run plato             # JavaScript Source Analysis
npm run tdd               # For Development
npm run test              # For Continuous Integration (single run)
npm run test-crossbrowser # Cross-browser test (Chrome, Firefox, IE, Opera, Safari, PhantomJS)
```

#### Build

```powershell
C:\Projects\Core.Dev\code\EZ.Store.Custom\Content\Themes\POS
gulp build
```

##### Documentation only

```powershell
gulp ngdocs
cd Docs/
http-server
```

### Tools

- [RealFaviconGenerator](http://realfavicongenerator.net/)
- [TinyPNG](https://tinypng.com/)
- [ConvertICO](http://convertico.com/)

## Packages

- [jQuery](https://jquery.com/)
- [HeadJS](https://github.com/headjs/headjs)
- [CPF/CNPJ](https://github.com/fnando/cpf_cnpj.js)
- [AngularJS](https://angularjs.org/)
  - [i18n](https://github.com/angular/bower-angular-i18n)
  - [UI Router](https://angular-ui.github.io/ui-router/)
  - [UI Mask](https://github.com/benmcosker/angular-ui-mask)
  - [Storage](https://github.com/gsklee/ngStorage)
  - [Toaster](http://jonsamwell.github.io/angular-auto-validate/)
  - [Auto Validate](https://github.com/jirikavi/AngularJS-Toaster)
  - [Image Fallback](https://github.com/dcohenb/angular-img-fallback)
  - [Busy](https://github.com/cgross/angular-busy)
  - [Prompt](https://github.com/cgross/angular-prompt)

### Pages

- [pace](https://github.com/HubSpot/pace/)
- [unveil.js](https://github.com/luis-almeida/unveil)
- [Bez](https://github.com/rdallasgray/bez)
- [iOSList](https://github.com/brianhadaway/iOSList)
- [jQuery Actual](https://github.com/dreamerslab/jquery.actual)
- [jQuery Scrollbar](https://github.com/gromo/jquery.scrollbar)
- [Select2](https://select2.github.io/)
- [classie](https://github.com/desandro/classie)
- [jQuery Validation](https://jqueryvalidation.org/)

## System requirements and browsers

### Browser

**Point of Sale** work with the 3 most recent versions of the following browsers. Make sure cookies and [JavaScript](http://enable-javascript.com/) are turned on for your browser.

> Note: see which version you're using [whatsmybrowser.org](http://www.whatsmybrowser.org/).

#### Computer

- **Chrome**
- **Firefox**
- **Internet Explorer, Microsoft Edge** (Windows only)
- **Safari** (Mac only)

Other browsers may work, but you might not be able to use all of the features.

#### Computer operating systems

- **Windows**: Windows 7 and up
- **Mac**: Yosemite (10.10) and up

### Android

You'll need Android version 4.1 or up.

### iPhone & iPad

You'll need iOS 7.0 or up.

### Resolutions

- Mobile - 320/425
- Tablet - 768
- Laptop - 1024/1440
